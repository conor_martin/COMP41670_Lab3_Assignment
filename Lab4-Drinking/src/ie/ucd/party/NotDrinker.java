package ie.ucd.party;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.Person;

public class NotDrinker extends Person {
	
	protected java.lang.String name;

	public NotDrinker(java.lang.String name) {
		this.name = name;
	}

	public boolean drink(Drink arg0) {
		if(arg0 instanceof AlcoholicDrink) {
			System.out.println(this.name + ": Hell no! I ain't drinking no " + arg0.getName());
			return false;
		}
		else {
			System.out.println(this.name + ": Mmmm! I love " + arg0.getName());
			return true;
		}
	}

	public boolean eat(Food arg0) {
		return true;
	}

}
