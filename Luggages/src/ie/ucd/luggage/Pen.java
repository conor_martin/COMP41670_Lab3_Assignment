package ie.ucd.luggage;

/**
 * Class representing a safe Object that implements the Item interface
 * @author Conor
 *
 */
public class Pen implements Item {

	protected String type;
	protected double weight;

	/**
	 * 
	 * @param newType Type of Pen 
	 * @param newWeight Weight of Pen
	 */
	public Pen(String newType, double newWeight) {
		this.type = newType;
		this.weight = newWeight;
	}

	public String getType() {
		return type;
	}

	public double getWeight() {
		return weight;
	}

	public boolean isDangerous() {
		return false;
	}

}
